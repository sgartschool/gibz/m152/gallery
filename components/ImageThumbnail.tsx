import type { Image } from "../pages"

export default (
    { image, assetPrefix, setFullscreenContent }:
    { image: Image, assetPrefix: string, setFullscreenContent: Function }
) => <img
    key={image.name}
    src={`${assetPrefix}/images/${encodeURIComponent(image.name)}/converted_400x300p.webp`}
    alt={image.name}
    srcSet={`
    ${assetPrefix}/images/${encodeURIComponent(image.name)}/converted_800x600p.webp 2x,
    ${assetPrefix}/images/${encodeURIComponent(image.name)}/converted_400x300p.webp 1x
    `}
    onClick={() => setFullscreenContent(image)}
/>