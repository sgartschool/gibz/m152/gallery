import { useState } from "react"
import type { Video } from "../pages"
import Head from "next/head"

export default (
    { video, assetPrefix, setFullscreenContent }:
    { video: Video, assetPrefix: string, setFullscreenContent: Function }
) => {
    const [ isHovering, setIsHovering ] = useState(false)

    if (!isHovering) return <>

        <img
            key={video.name}
            src={`${assetPrefix}/videos/${encodeURIComponent(video.name)}/thumbnail_400x300p.webp`}
            alt={video.name}
            srcSet={`
            ${assetPrefix}/videos/${encodeURIComponent(video.name)}/thumbnail_800x600p.webp 2x,
            ${assetPrefix}/videos/${encodeURIComponent(video.name)}/thumbnail_400x300p.webp 1x
            `}
            onClick={() => setFullscreenContent(video)}
            onMouseEnter={() => setIsHovering(true)}
        />

        {/* preload video */}
        <video preload="auto" style={{ display: "none" }}>
            <source
                src={`${assetPrefix}/videos/${encodeURIComponent(video.name)}/thumbnail_animated_800x600p.webm`}
                type="video/webm"
                media="min-device-pixel-ratio: 2"
            /> 
            <source
                src={`${assetPrefix}/videos/${encodeURIComponent(video.name)}/thumbnail_animated_400x300p.webm`}
                type="video/webm"
            /> 
        </video>

    </>

    return (
        <video autoPlay={true} loop={true} muted={true}
            onMouseLeave={() => setIsHovering(false)}
            onClick={() => setFullscreenContent(video)}
        >
            <source
                src={`${assetPrefix}/videos/${encodeURIComponent(video.name)}/thumbnail_animated_800x600p.webm`}
                type="video/webm"
                media="min-device-pixel-ratio: 2"
            /> 
            <source
                src={`${assetPrefix}/videos/${encodeURIComponent(video.name)}/thumbnail_animated_400x300p.webm`}
                type="video/webm"
            /> 
        </video>
    )
}