import styles from '../styles/components/navigation.module.scss'
import { useRouter } from "next/router"

import Icon from '@mdi/react'
import { mdiFileDocument, mdiHome, mdiLockAlertOutline, mdiRadioboxMarked, mdiTestTube } from '@mdi/js'

const pagePrefix = process.env.NODE_ENV == "development" ? "" : "/sj_21_22/infa3a_05"

export default () => {
    const pathname = useRouter().pathname

    const NavItem = ({ children, path }: { children: any, path: string }) => <div 
        onMouseDown={e => e.button == 2 ? window.open(path, "_blank") : window.location.href = `${pagePrefix}${path}`}
        className={pathname.endsWith(path) || pathname.endsWith(`${path}/`) ? styles.active : ""}
    >
        {children}
    </div>

    return <div className={styles.navigation}>
        <img src={`${pagePrefix}/favicon.webp`} alt="" />

        <NavItem path={`/`}>
            <Icon path={mdiHome} size={1}/>
        </NavItem>

        <NavItem path={`/abstract`}>
            <Icon path={mdiTestTube} size={1}/>
        </NavItem>

        <NavItem path={`/live`}>
            <Icon path={mdiRadioboxMarked} size={1}/>
        </NavItem>

        <NavItem path={`/impressum`}>
            <Icon path={mdiFileDocument} size={1}/>
        </NavItem>

    </div>

}