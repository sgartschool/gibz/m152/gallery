import styles from '../styles/components/fullscreenViewer.module.scss'

import { Image, Video } from '../pages'
import { MouseEventHandler, useEffect, useRef } from 'react'

import Icon from '@mdi/react'
import { mdiClose, mdiDownload } from '@mdi/js'

// import VideoJS from './VideoJS'

const download = (path: string, filename: string) => {
    // Create a new link
    const anchor = document.createElement('a');
    anchor.href = path;
    anchor.download = filename;

    // Append to the DOM
    document.body.appendChild(anchor);

    // Trigger `click` event
    anchor.click();

    // Remove element from DOM
    document.body.removeChild(anchor);
};

export default ({ image, video, close, assetPrefix }: { image?: Image, video?: Video, close: Function, assetPrefix: string }) => {
    useEffect(() => {
        window.addEventListener("keydown", e => e.key == "Escape" || e.key == "Backspace" && close())
    }, [])

    useEffect(() => {
        if (!video) return

        const script = document.createElement("script");
        script.src = "https://cdn.dashjs.org/latest/dash.all.min.js";
        document.body.appendChild(script);

        console.log(`${assetPrefix}/videos/${encodeURIComponent(video.name)}/manifest.mpd`)

        setTimeout(() => {
            var player = dashjs.MediaPlayer().create();
            player.initialize(document.querySelector("video"), `${assetPrefix}/videos/${encodeURIComponent(video.name)}/manifest.mpd`, true);
        }, 300);

    }, [ true ])

    return <div className={styles.background} id="fullscreenBackground">
        
        {/* TODO: loop */}
        {image && <img
            src={`${assetPrefix}/images/${encodeURIComponent(image.name)}/converted_480p.webp`}
            alt={image.name}
            srcSet={`
                ${assetPrefix}/images/${encodeURIComponent(image.name)}/converted_2160p.webp  3840w,
                ${assetPrefix}/images/${encodeURIComponent(image.name)}/converted_1080p.webp  1920w,
                ${assetPrefix}/images/${encodeURIComponent(image.name)}/converted_720p.webp   1280w,
                ${assetPrefix}/images/${encodeURIComponent(image.name)}/converted_480p.webp   853w
            `}
            sizes="90vw"
        />}

        {video && typeof window != "undefined" && <div className={styles.player}>
            <video src="" controls></video>
        </div>}
        

        <div className={styles.buttons}>
            <div onClick={close as MouseEventHandler}>
                <Icon path={mdiClose} size={1.1} />
            </div>

            <div onClick={async () => {
                const element = image || video
                if (!element) return

                download(
                    `${assetPrefix}/${image ? "images" : "videos"}/${encodeURIComponent(element.name)}/${element.originalFilePath}`,
                    `${element.name}.${element.originalFilePath.split(".")[1]}`
                )
            }}>
                <Icon path={mdiDownload} size={1.1} />
            </div>
        </div>
    </div>
}