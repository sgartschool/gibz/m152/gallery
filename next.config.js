/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  trailingSlash: true,
  assetPrefix: process.env.NODE_ENV == "development" ? "/" : "/sj_21_22/infa3a_05"
}

module.exports = nextConfig
