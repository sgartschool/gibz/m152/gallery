import styles from '../styles/Home.module.scss'

import { useState } from 'react'

import FullscreenViewer from '../components/FullscreenViewer'

export interface Image {
  type: "image";
  name: string;
  hasRaw: string | false;
  originalFilePath: string;
}

export interface Video {
  type: "video";
  name: string;
  originalFilePath: string;
}

import fs from 'fs/promises'
import VideoThumbnail from '../components/VideoThumbnail';
import ImageThumbnail from '../components/ImageThumbnail';

export const getStaticProps = async () => {
  const imagePaths = (await fs.readdir('public/images'))
  
  const images: Array<Image> = []
  for (const i in imagePaths) {
    const name = imagePaths[i]
    images.push({
      type: "image",
      name,
      hasRaw: (await fs.readdir(`public/images/${name}`)).find(filename => filename.match(/^raw.*$/)) || false,
      originalFilePath: (await fs.readdir(`public/images/${name}`)).find(filename => filename.match(/^original.*$/)) as string
    })
  }
  console.log(images)

  const videosPath = (await fs.readdir('public/videos'))
  const videos: Array<Video> = []
  for (const i in videosPath) {
    const name = videosPath[i]
    videos.push({
      type: "video",
      name,
      originalFilePath: (await fs.readdir(`public/videos/${name}`)).find(filename => filename.match(/^original.*$/)) as string
    })
  }
  console.log(videos)
  
  return {
    props: {
      buildTimestamp: Date.now(),
      assetPrefix: process.env.NODE_ENV == "development" ? "" : "/sj_21_22/infa3a_05",
      images,
      videos
    }
  }
}

const Home = ({ images, videos, assetPrefix }: { images: Array<Image>, videos: Array<Video>, assetPrefix: string }) => {
  const [ fullscreenContent, setFullscreenContent ] = useState<null | Image | Video>(null)

  return (
    <div className={styles.container}>

      {fullscreenContent && fullscreenContent.type == "image" && <FullscreenViewer close={() => setFullscreenContent(null)} image={fullscreenContent as Image} assetPrefix={assetPrefix}/>}
      {fullscreenContent && fullscreenContent.type == "video" && <FullscreenViewer close={() => setFullscreenContent(null)} video={fullscreenContent as Video} assetPrefix={assetPrefix}/>}

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a>Lux Gallery</a>!
        </h1>

        <div className={styles.grid}>
          
          {videos.map(video => 
            <VideoThumbnail key={video.name} video={video} assetPrefix={assetPrefix} setFullscreenContent={setFullscreenContent} />
          )}

          {images.map(image => 
            <ImageThumbnail key={image.name} image={image} assetPrefix={assetPrefix} setFullscreenContent={setFullscreenContent} />
          )}

        </div>
      </main>

      <footer className={styles.footer}>
        <div>
          Powered by Switzerland's Mountains
          <br/>
          <br/>
          <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>
        </div>
      </footer>
    </div>
  )
}

export default Home
