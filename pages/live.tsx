import styles from '../styles/Home.module.scss'

import { useState } from 'react'

export interface Image {
  type: "image";
  name: string;
  hasRaw: string | false;
  originalFilePath: string;
}

export interface Video {
  type: "video";
  name: string;
  originalFilePath: string;
}

import FullscreenViewer from '../components/FullscreenViewer'

export const getStaticProps = async () => {
  return {
    props: {
      buildTimestamp: Date.now(),
      assetPrefix: process.env.NODE_ENV == "development" ? "/abstract" : "/sj_21_22/infa3a_05/abstract"
    }
  }
}

const Home = ({ images, videos, assetPrefix }: { images: Array<Image>, videos: Array<Video>, assetPrefix: string }) => {
  const [ fullscreenContent, setFullscreenContent ] = useState<null | Image | Video>(null)

  return (
    <div className={styles.container}>

      {fullscreenContent && fullscreenContent.type == "image" && <FullscreenViewer close={() => setFullscreenContent(null)} image={fullscreenContent as Image} assetPrefix={assetPrefix}/>}
      {fullscreenContent && fullscreenContent.type == "video" && <FullscreenViewer close={() => setFullscreenContent(null)} video={fullscreenContent as Video} assetPrefix={assetPrefix}/>}

      <main className={styles.main}>
        <div className={styles.grid} style={{ width: "100%" }}>

          <img style={{ width: "640px", maxWidth: "70%" }} src="https://live.any.gay/" alt="" />

        </div>
      </main>

      <footer className={styles.footer}>
        <div>
          Powered by Switzerland's Mountains
          <br/>
          <br/>
          <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>
        </div>
      </footer>
    </div>
  )
}

export default Home
