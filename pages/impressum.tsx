import styles from '../styles/Home.module.scss'

const Impressum = () => <div>
    <main className={styles.main}>
        <div className={styles.impressum}>
            <h3>Allgemein</h3>
            <div>
                <div>Autoren:</div>
                <div>Luisa furrer</div>
                <div>Samuel Gartmann</div>
                <div>Yanik Ammann</div>
                <br/>
                <div>Schule:</div>
                <div>GIBZ Gewerblich-industrielles<br/>Bildungszentrum Zug</div>
                <div>Baarerstrasse 100</div>
                <div>6301 Zug</div>
            </div>

            <h3>Verwendete Tools</h3>
            <div>
            Für die Konvertierung der Bilder bzw. Fotos, der Videos sowie das Hinzufügen<br/>der Audi-Spuren und Untertiteln haben wir ffmpeg verwendet.
                <br/>
            Um das Video zusammenzuschneiden haben wir https://www.veed.io/ eingesetz.
                <br/>
            Um die Controls bei den Videos zu steuern haben wir die Dash.js API verwendet.
                <br/>
                <br/>
            </div>

            <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />Alle Inhalte dieser Webseite sind unter<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License lizensiert</a>.
        </div>
    </main>

    <footer className={styles.footer}>
        <div>
            Powered by Switzerland's Mountains
            <br/>
            <br/>
            <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>
        </div>
    </footer>
</div>

export default Impressum