import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Head from 'next/head'

import Navigation from '../components/Navigation'

function MyApp({ Component, pageProps }: AppProps) {
  return <>
    <Head>
      <title>Lux Gallery</title>
      <link rel="shortcut icon" href="https://m152.gibz-informatik.ch/sj_21_22/infa3a_05/favicon.webp" type="image/webp" />
    </Head>

    <Navigation/>
    <Component {...pageProps} />
  </>
}

export default MyApp
